﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CsharpHome.Controllers
{
    public class CirclePerimeterController : Controller
    {
        private const double Pi = Math.PI;
        // GET: CirclePerimeter
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(double circleRadius)
        {
            ViewBag.CirclePerimeter = 2 * Pi * circleRadius;
            ViewBag.CircleArea = Pi * Math.Pow(circleRadius, 2);
            
            return View();
        }
    }
}
