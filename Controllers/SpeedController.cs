﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CsharpHome.Controllers
{
    public class SpeedController : Controller
    {
        // GET: Speed
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(double distance, double time)
        {
            ViewBag.SpeedDistance = distance;
            ViewBag.SpeedTime = time;
            ViewBag.SpeedKmph = distance / time;
            ViewBag.SpeedMph = ViewBag.SpeedKmph * 0.621371;

            return View();
        }
    }
}
