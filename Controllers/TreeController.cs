﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CsharpHome.Controllers
{
    public class TreeController : Controller
    {
        // GET: Tree
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(int number, int width)
        {
            ViewBag.TreeNumber = number;
            ViewBag.TreeWidth = width;
            return View();
        }
    }
}
