﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CsharpHome.Controllers
{
    public class CalculatorController : Controller
    {
        // GET: Calculator
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(double firstNumber, double secondNumber, string command)
        {
            switch (command) {
                case "sum": ViewBag.CalculatorTotal = firstNumber + secondNumber;
                    break;
                case "minus": ViewBag.CalculatorTotal = firstNumber - secondNumber;
                    break;
                case "multiple":
                    ViewBag.CalculatorTotal = firstNumber * secondNumber;
                    break;
                case "divide":
                    ViewBag.CalculatorTotal = firstNumber / secondNumber;
                    break;
                default:
                    break;
            }

            return View();
        }
    }
}
