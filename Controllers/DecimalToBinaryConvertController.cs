﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CsharpHome.Controllers
{
    public class DecimalToBinaryConvertController : Controller
    {
        // GET: DecimalToBinaryConvert
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(decimal digit)
        {
            ViewBag.DecimalToBinaryConvertValue = digit;
            decimal doublePartOfDecimal = (digit - (int)digit);

            ViewBag.DecimalToBinaryConvertResultIntPart = DigitToString(digit, 2);
            ViewBag.DecimalToBinaryConvertResultDecimalPart = StringToBinary(doublePartOfDecimal, doublePartOfDecimal.ToString().Length - 2);

            return View();
        }

        public static string StringToBinary(decimal data, int dataCount)
        {
            var multipleValue = data * (decimal)Math.Pow(10, dataCount);
            return DigitToString(multipleValue, 2);
        }

        public static string DigitToString(decimal variable, int type)
        {
            return Convert.ToString((int)variable, type);
        }
    }
}
