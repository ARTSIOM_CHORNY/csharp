﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CsharpHome.Controllers
{
    public class SphereVolumeController : Controller
    {
        private const double Pi = Math.PI;

        // GET: SphereVolume
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(double sphereRadius)
        {
            ViewBag.SphereSurface = 4 * Pi * Math.Pow(sphereRadius, 2);
            ViewBag.SphereVolume = (4 * Pi * Math.Pow(sphereRadius, 3)) / 3;

            return View();
        }
    }
}
