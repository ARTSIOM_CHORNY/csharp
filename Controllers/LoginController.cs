﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CsharpHome.Controllers
{
    public class LoginController : Controller
    {
        private string userFromPair = "";
        private int attemptFromPair  = 0;
        Dictionary<string, int> usersAttemptsList = new Dictionary<string, int>();
        List<string> userAttemptsPair = new List<string>();

        // GET: Login
        public ActionResult Index()
        {
            //Response.Cookies.Add(CreateUserCookie());
            //if (Request.Cookies["UserCookie"] == null)

            /*
             idea: set User as cookie name; Attemps as its value
             */

                Response.Cookies.Add(CreateUserCookie());
            return View();
        }

        [HttpPost]
        public ActionResult Index(string username, string password)
        {
            ViewBag.LoginUsername = username;
            ViewBag.LoginPassword = password;
            ViewBag.LoginRejected = false;            

            if (Session["UsersAttemps"] != null)
            {
                userAttemptsPair = Session["UsersAttemps"].ToString().Split(';').ToList();
                userAttemptsPair.RemoveAll(item => String.IsNullOrEmpty(item));
                foreach (var item in userAttemptsPair)
                {
                    userFromPair = item.Split(',')[0];
                    attemptFromPair = Int32.Parse(item.Split(',')[1]);

                    if (!usersAttemptsList.Keys.Contains(userFromPair))
                        usersAttemptsList.Add(userFromPair, attemptFromPair);
                    else if (usersAttemptsList[userFromPair] < attemptFromPair)
                        usersAttemptsList[userFromPair] = attemptFromPair;
                }
            }

            if (usersAttemptsList.Keys.Contains(username))
                usersAttemptsList[username] = usersAttemptsList[username] + 1;
            else
                usersAttemptsList.Add(username, 0);

            if (usersAttemptsList[username] == 3)
            {
                usersAttemptsList[username] = 0;
                ViewBag.LoginRejected = true;
            }
            else {
                ViewBag.LoginRejected = false;
            }

            foreach (var item in usersAttemptsList)
                Session["UsersAttemps"] = Session["UsersAttemps"] + ";" + item.Key + "," + item.Value;

            return View();
        }
        
        private HttpCookie CreateUserCookie()
        {
            HttpCookie UserCookies = Request.Cookies["UserCookie"];
            UserCookies = new HttpCookie("UserCookie");
            UserCookies.Value = Guid.NewGuid().ToString();
            Session["loginCount"] = 0;
            return UserCookies;
        }
        
    }
}
