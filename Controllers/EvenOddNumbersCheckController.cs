﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CsharpHome.Controllers
{
    public class EvenOddNumbersCheckController : Controller
    {
        // GET: EvenOddNumbersCheck
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(int firstNumber, int secondNumber)
        {
            if (((firstNumber % 2) ^ (secondNumber % 2)) == 0)
                if ((secondNumber % 2) > 0)
                    ViewBag.EvenOddNumbersResult = "Numbers are both odd";
                else
                    ViewBag.EvenOddNumbersResult = "Numbers are both even";
            else
                ViewBag.EvenOddNumbersResult = "Numbers are different (odd and even)";
            return View();
        }
    }
}
