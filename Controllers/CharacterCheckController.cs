﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CsharpHome.Controllers
{
    public class CharacterCheckController : Controller
    {
        // GET: CharacterCheck
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(char character)
        {
            ViewBag.CharacterValue = character;

            if ("aeiouAEIOU".IndexOf(character) > -1)
                ViewBag.CharacterType = "It's a vowel";
            else if ("0123456789".IndexOf(character) > -1)
                ViewBag.CharacterType = "It's a digit";
            else
                ViewBag.CharacterType = "It is neither a vowel nor a digit";

            return View();
        }
    }
}
