﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CsharpHome.Controllers
{
    public class FunctionController : Controller
    {
        private const int HighGate = 5;
        private const int LowGate = -5;

        // GET: Counter
        [HttpGet]
        public ActionResult Index()
        {
            var calculatedValuesDictionary = new List<KeyValuePair<int, int>>();

            int functionParameter = LowGate;
            int functionResult = 0;

            for (var i = 0; i < HighGate - LowGate + 1; i++)
            {
                functionResult = functionParameter * functionParameter + 2 * functionParameter + 1;
                calculatedValuesDictionary.Insert(i, new KeyValuePair<int, int>(functionParameter, functionResult));
                functionParameter++;
            }

            ViewBag.FunctionValues = calculatedValuesDictionary;

            return View();
        }
        /*
        [HttpPost]
        public ActionResult Index()
        {
            var calculatedValuesDictionary = new List<KeyValuePair<int, double>>();

            var functionParameter  = LowGate;
            var functionResult = 0;

            for (var i = 0; i < HighGate - LowGate + 1; i++)
            {
                functionResult = functionParameter * functionParameter + 2 * functionParameter + 1;
                calculatedValuesDictionary.Insert(i, new KeyValuePair<int, double>(functionParameter, functionResult));
                functionParameter++;
            }

            ViewBag.FunctionValues = calculatedValuesDictionary;            
            
            return View();
        }*/
    }
}
