﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CsharpHome.Controllers
{
    public class ReverseController : Controller
    {
        // GET: Counter
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string letters)
        {
            ViewBag.ReversedLetters = letters.Reverse();
            return View();
        }
    }
}
